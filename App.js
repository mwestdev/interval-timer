import React from "react";
import { StyleSheet, Text, View } from "react-native";

import Header from "./components/Header";
import Timer from "./components/Timer";

export default function App() {
    return (
        <View style={styles.screen}>
            <Header title="Interval Timer" />
            <Timer />
        </View>
    );
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
    },
});
