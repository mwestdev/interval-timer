import React, { useState, useEffect } from "react";
import {
    View,
    Text,
    StyleSheet,
    Button,
    TouchableWithoutFeedback,
    Keyboard,
} from "react-native";

import Card from "../components/Card";
import Input from "../components/Input";
import Colors from "../constants/colors";

const Timer = (props) => {
    const [initialTime, setInitialTime] = useState(0);
    const [startTimer, setStartTimer] = useState(false);
    const [enteredValue, setEnteredValue] = useState("");
    const [confirmed, setConfirmed] = useState(false);
    const [selectedNumber, setSelectedNumber] = useState();

    const confirmTimerHandler = () => {
        setStartTimer(false);
        const chosenNumber = parseInt(enteredValue);
        if (isNaN(chosenNumber) || chosenNumber <= 0 || chosenNumber > 3600) {
            return;
        }
        setConfirmed(true);
        setInitialTime(parseInt(enteredValue * 60));
        setEnteredValue("");
    };

    const resetInputHandler = () => {
        setInitialTime(0);
        setStartTimer(false);
        setEnteredValue("");
        setConfirmed(false);
    };

    const numberInputHandler = (inputText) => {
        setEnteredValue(inputText.replace(/[^0-9]/g, ""));
    };

    const pauseTimerHandler = () => {
        startTimer ? setStartTimer(false) : setStartTimer(true);
    };

    useEffect(() => {
        if (initialTime > 0 && startTimer && confirmed) {
            console.log("initialTime", minutes, ":", seconds);
            const timer = setTimeout(() => {
                setInitialTime(initialTime - 1);
            }, 1000);
            return () => clearTimeout(timer);
        }

        if (initialTime === 0 && startTimer) {
            console.log("done");
            setStartTimer(false);
        }
    }, [initialTime, startTimer, confirmed]);

    var minutes = ("00" + Math.floor(initialTime / 60)).slice(-2);
    var seconds = ("00" + (initialTime - minutes * 60)).slice(-2);

    let timer;

    if (confirmed) {
        timer = (
            <View style={styles.timerContainer}>
                <Text style={styles.timer}>
                    {minutes}:{seconds}
                </Text>
                <View style={styles.buttonContainer}>
                    <View style={styles.button}>
                        <Button
                            title={startTimer ? "Pause" : "Play"}
                            onPress={pauseTimerHandler}
                            color={Colors.lightGrey}
                        />
                        <Button
                            title="Favourite"
                            onPress={() => {}}
                            color={Colors.grey}
                        />
                    </View>
                </View>
            </View>
        );
    }
    return (
        <TouchableWithoutFeedback
            onPress={() => {
                Keyboard.dismiss();
            }}>
            <View style={styles.screen}>
                <Card style={styles.inputContainer}>
                    <Text>Choose an Interval</Text>
                    <Input
                        style={styles.input}
                        blurOnSubmit
                        autoCapitalize="none"
                        autoCorrect={false}
                        keyboardType="number-pad"
                        maxLength={2}
                        onChangeText={numberInputHandler}
                        value={enteredValue}
                    />
                    <View style={styles.buttonContainer}>
                        <View style={styles.button}>
                            <Button
                                title="Reset"
                                onPress={resetInputHandler}
                                color={Colors.grey}
                            />
                        </View>
                        <View style={styles.button}>
                            <Button
                                title="Confirm"
                                onPress={confirmTimerHandler}
                                color={Colors.darkGrey}
                            />
                        </View>
                    </View>
                </Card>
                {timer}
            </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        padding: 10,
        alignItems: "center",
    },
    inputContainer: {
        width: 300,
        maxWidth: "80%",
        alignItems: "center",
    },
    buttonContainer: {
        flexDirection: "row",
        width: "100%",
        justifyContent: "space-between",
        paddingHorizontal: 15,
    },
    button: {
        width: 100,
    },
    input: {
        width: 50,
        textAlign: "center",
    },
    timerContainer: {
        alignItems: "center",
        paddingTop: 20,
    },
    timer: {
        fontWeight: "bold",
        fontSize: 30,
    },
});

export default Timer;
