import React from "react";
import { View, Text, StyleSheet } from "react-native";
import PropTypes from "prop-types";
import Colors from "../constants/colors";

const Header = (props) => {
    return (
        <View style={styles.header}>
            <Text style={styles.headerTitle}>{props.title}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    header: {
        width: "100%",
        height: 90,
        paddingTop: 36,
        backgroundColor: Colors.darkGrey,
        alignItems: "center",
        justifyContent: "center",
    },
    headerTitle: {
        color: "white",
        fontSize: 18,
    },
});

Header.propTypes = {
    title: PropTypes.string,
};

export default Header;
