export default {
    darkGrey: "#404552",
    darkerGrey: "#383c4a",
    grey: "#4b5162",
    blue: "#5294e2",
    lightGrey: "#7c818c",
};
